import { of, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss'],
})
export class ConnectComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    // this.getConnectorPoints(this.obj1Props.subscribe((val) => {return val}), this.obj2Props.subscribe((val) => {return val}));
    console.log(this.obj2Props);
  }
  public configStage: Observable<any> = of({
    width: 600,
    height: 600,
  });

  obj1Props: Observable<any> = of({
    x: 100,
    y: 100,
    radius: 70,
    fill: 'red',
    stroke: 'black',
    strokeWidth: 4,
    draggable: true,
  });
  // public obj1 = new Konva.Circle({ ...this.obj1Props })

  obj2Props: Observable<any> = of({
    x: 300,
    y: 100,
    radius: 70,
    fill: 'blue',
    stroke: 'black',
    strokeWidth: 4,
    draggable: true,
  });
  // const obj2= new Konva.Circle({ ...this.obj2Props });


  public line: Observable<any> = of({
    // points: [
    //   this.obj1Props.subscribe((val) => {return val.x}),
    //   this.obj1Props.subscribe((val) => {return val.y}),
    //   this.obj2Props.subscribe((val) => {return val.x}),
    //   this.obj2Props.subscribe((val) => {return val.y}),
    // ],
    points: [
      100,
      100,
      300,
      100,
    ],
    stroke: 'red',
    tension: 1,
    pointerLength : 10,
    pointerWidth : 12
  });
  // public line: Observable<any> = of(this.getConnectorPoints(
  //   this.obj1Props.subscribe((val) => {return val}),
  //   this.obj2Props.subscribe((val) => {return val})
  // ));

  // getConnectorPoints(from, to) {
  //   const dx = to.x - from.x;
  //   const dy = to.y - from.y;
  //   let angle = Math.atan2(-dy, dx);

  //   const radius = 50;

  //   return [
  //     from.x + -radius * Math.cos(angle + Math.PI),
  //     from.y + radius * Math.sin(angle + Math.PI),
  //     to.x + -radius * Math.cos(angle),
  //     to.y + radius * Math.sin(angle),
  //   ];
  // }
  // public line2 = new Konva.Line({
  //   points: [73, 70, 340, 23, 450, 60, 500, 20],
  //   stroke: 'red',
  //   tension: 1,
  //   pointerLength : 10,
  //   pointerWidth : 12
  // });

  // public gragMove(){
  //   this.obj1.on('dragmove', updateLine);
  //   this.obj2.on('dragmove', updateLine);
  // }
  // this.gragMove()
  // function updateLine() {
  //   line.points([obj1.x(), obj1.y(), obj2.x(), obj2.y]);
  //   layer.batchDraw();
  // }
}
