import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

// Import KonvaModule
import { KonvaModule } from "ng2-konva";
import { CanvasComponent } from './canvas/canvas.component';
import { ConnectComponent } from './connect/connect.component';
import { StarsComponent } from './stars/stars.component';
@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    ConnectComponent,
    StarsComponent
  ],
  imports: [
    BrowserModule,
    KonvaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
