import { Component, OnInit, ViewChild } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable, of } from 'rxjs';
import { KonvaComponent } from 'ng2-konva';

declare const Konva: any;
let ng: any;

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss'],
})
export class CanvasComponent implements OnInit {
  @ViewChild('stage') stage: KonvaComponent;
  @ViewChild('layer') layer: KonvaComponent;
  @ViewChild('dragLayer') dragLayer: KonvaComponent;
  @ViewChild('circle') circle: KonvaComponent;

  public width = 800;
  public height = 200;
  public list: Array<any> = [];

  public configStage: Observable<any> = of({
    width: this.width,
    height: this.height,
  });
  constructor() {}

  ngOnInit(): void {}

  public configCircle1: Observable<any> = of({
    x: 100,
    y: 100,
    radius: 70,
    fill: 'red',
    stroke: 'black',
    strokeWidth: 4,
    // draggable :true
  });
  public configCircle2: Observable<any> = of({
    x: 300,
    y: 100,
    radius: 70,
    fill: 'blue',
    stroke: 'black',
    strokeWidth: 4,
    // draggable :true
  });
  // this.configCircle.subscribe(val => console.log(val))
  public line: Observable<any> = of({
    points: [
      10,10,
      50,50
    ],
    stroke: 'green',
    strokeWidth: 10,
    lineJoin: 'round',
  });
  public handleClick(component) {
    console.log(component);
  }

  generateLine() {
    let line
    this.getConnectorPoints(
      this.configCircle1.subscribe((val) => val ),
      this.configCircle2.subscribe((val) => val)
    );
  }
  getConnectorPoints(from, to) {
    const dx = to.x - from.x;
    const dy = to.y - from.y;
    let angle = Math.atan2(-dy, dx);

    const radius = 50;

    return [
      from.x + -radius * Math.cos(angle + Math.PI),
      from.y + radius * Math.sin(angle + Math.PI),
      to.x + -radius * Math.cos(angle),
      to.y + radius * Math.sin(angle),
    ];
  }
}
